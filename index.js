// start BT1
function findMinN(){
    var sum=0;
    var n=0;
    while(sum<=10000){
        n++;
        sum=sum+n;
    }
    document.getElementById('findMinN').innerHTML=` Số nguyên dương nhỏ nhất: <b>${n}</b>`
}
// end BT1

// start BT2
function sumMathPow() {
    var n=document.getElementById('n').value*1;
    if (n<=0 || Number.isInteger(n)==false) {return alert('Vui lòng nhập lại số n NGUYÊN DƯƠNG và >=1')}
    var x=document.getElementById('x').value*1;
    var sum=0;
    for (var i=1; i<=n; i++){
        sum=sum+Math.pow(x,i);
    }
    document.getElementById('sumMathPow').innerHTML=`Tổng: <b>${sum}</b> `
}
// end BT2

// start BT3
function factorialCalculation(){
    var n2=document.getElementById('n2').value*1;
    if (n2<=0 || Number.isInteger(n2)==false) {return alert('Vui lòng nhập lại số n NGUYÊN DƯƠNG và >=1')}
    // xét trường hợp ko hợp lệ
    var multiply=1;
    for (var i=1; i<=n2; i++){
        console.log(i);
        multiply=multiply*i;
        console.log(multiply);
    }
    document.getElementById('factorialCalculation').innerHTML=`Giai thừa: <b>${multiply}</b>`
}
// end BT3

// start BT4
function createDiv(){
    var result='';
    for (var i=1; i<=10; i++){
        if(i%2==0){
            result=result+' <div class="col-12" style="background-color:#0B4B72;height:30px;">Div chẵn</div>';
        }else{
            result=result+' <div class="col-12" style="background-color:#FCD716;height:30px;color:black;">Div lẻ</div>';        }
    }
    document.getElementById('createDiv').innerHTML=`${result}`;
}
// end BT4

// start BT5
function primeNumber(n){
    if (n==2 || n==3){
        return true;
    }else{ 
        for (var i=2; i<=Math.sqrt(n); i++){
            if (n%i==0){
                return false
            }
        }
    }
    return true;
}

function listPrimeNumber(){
    var n3=document.getElementById('n3').value*1;
    if (n3<0) {return alert('Vui lòng nhập lại số n >= 0')}

    var result='';
    for (var i=2; i<=n3; i++){
        if (primeNumber(i)==true){
            result=result+' '+i;
        }
    }
    console.log(result);
    document.getElementById('listPrimeNumber').innerHTML=`${result}`;
}
// end BT5

// start BT6
function findOddEven(){
    var odd='Số chẵn:';
    var even='Số lẻ:';
    for(var i=1; i<100; i++){
        if(i%2==0){
            odd=odd+` `+i;
        } else {
            even=even+' '+i;
        }
    };
    document.getElementById('findOddEven').innerHTML=`<b style="color:#C20000">${odd}</b>
                                                    <br/><b style="color:#002795">${even}</b>`;
}
// end BT6

// start BT7
function divisibleBy3(){
    var count=0;
    for (var i=0; i<=1000; i++){
        if (i%3==0){
            count++;
        }
    };
    document.getElementById('divisibleBy3').innerHTML=` Số chia hết cho 3 nhỏ hơn 1000: &nbsp <b>${count} số</b>`
}
// end BT7